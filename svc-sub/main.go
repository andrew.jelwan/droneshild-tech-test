package main

import (
	"fmt"
	"log"
	"net/http"
	"svc-sub/server"

	"github.com/gorilla/mux"
)

type Subscriber struct {
	muxRouter *mux.Router
}

func NewSubscriber() *Subscriber {
	return &Subscriber{
		muxRouter: mux.NewRouter(),
	}
}

func main() {
	fmt.Println("Starting Redis Subscriber")

	subscriber := NewSubscriber()

	subscriber.muxRouter.HandleFunc("/", server.WsEndpoint)
	subscriber.muxRouter.HandleFunc("/echo", server.EchoEndpoint)

	http.Handle("/", subscriber.muxRouter)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

package server

import (
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
)

func mockRedis() *miniredis.Miniredis {
	rmock, err := miniredis.Run()
	rmock.RequireAuth("test")

	if err != nil {
		panic(err)
	}
	return rmock
}

func NewMockRedisHandler(ctx context.Context, address string, password string) *RedisHandler {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       0,
	})

	return &RedisHandler{
		client: client,
	}
}

func PublishMessages(mockRedis *miniredis.Miniredis, channel string, count int) {
	for i := 0; i < count; i++ {
		mockRedis.Publish(channel, "test")
		time.Sleep(time.Second * 1)
	}
}

func TestClientFail(t *testing.T) {
	mockRedisServer := mockRedis()
	ctx := context.Background()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	mockRedisServer.SetError("Mock Error")
	err := redisHandler.PingClient(ctx)
	assert.EqualError(t, err, "Mock Error")
	mockRedisServer.SetError("")
}

func TestClientSuccess(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	err := redisHandler.PingClient(ctx)
	assert.NoError(t, err)
}

func TestSubscriberFail(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	mockRedisServer.SetError("Mock Error")
	err := redisHandler.Subscribe(ctx, "test")
	assert.EqualError(t, err, "Mock Error")
	mockRedisServer.SetError("")
}

func TestSubscriberSuccess(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	err := redisHandler.Subscribe(ctx, "test")
	assert.NoError(t, err)
}

func TestListenForConnectionClose(t *testing.T) {
	closeChan := make(chan error, 1)

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
		}

		log.Println("Client Connected...")

		go listenForConnectionClose(ws, closeChan)

	}))
	defer s.Close()

	u := "ws" + strings.TrimPrefix(s.URL, "http")
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}

	ws.WriteMessage(websocket.CloseMessage, []byte{})

	err = <-closeChan
	if err != nil {
		ws.Close()
		t.Log(err)
		t.Log("Client Disconnected")
	}

}

func TestReadRedisChannel(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")

	err := redisHandler.Subscribe(ctx, "test")
	if err != nil {
		t.Fatal(err)
		return
	}
	channel := redisHandler.topic.Channel()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
		}

		log.Println("Client Connected...")

		go readRedisChannel(channel, ws)

	}))
	defer s.Close()

	u := "ws" + strings.TrimPrefix(s.URL, "http")
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}

	go PublishMessages(mockRedisServer, "test", 10)

	for i := 0; i < 5; i++ {
		_, p, err := ws.ReadMessage()
		if err != nil {
			t.Fatalf("%v", err)
		}
		log.Print(string(p))
	}
	ws.WriteMessage(websocket.CloseMessage, []byte{})
}

func TestReadQueue(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
		}

		log.Println("Client Connected...")

		go redisHandler.ReadQueue(ctx, "test", ws)

	}))
	defer s.Close()

	u := "ws" + strings.TrimPrefix(s.URL, "http")
	ws, _, err := websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}

	go PublishMessages(mockRedisServer, "test", 10)

	for i := 0; i < 5; i++ {
		_, p, err := ws.ReadMessage()
		if err != nil {
			t.Fatalf("%v", err)
		}
		log.Print(string(p))
	}
	ws.WriteMessage(websocket.CloseMessage, []byte{})
}

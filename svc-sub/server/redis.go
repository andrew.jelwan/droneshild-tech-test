package server

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

type RedisHandler struct {
	client *redis.Client
	topic  *redis.PubSub
}

func NewRedisHandler(ctx context.Context, address string, password string) *RedisHandler {

	handler := &RedisHandler{
		client: redis.NewClient(&redis.Options{
			Addr:     address,
			Password: password,
			DB:       0,
		}),
	}

	return handler
}

func (r *RedisHandler) Subscribe(ctx context.Context, topic string) error {
	r.topic = r.client.Subscribe(ctx, topic)
	err := r.PingTopic(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *RedisHandler) PingClient(ctx context.Context) error {
	err := r.client.Ping(ctx).Err()
	if err != nil {
		log.Printf("Redis not responding retry in 3 seconds err: %s", err)

		time.Sleep(time.Second * 3)

		err := r.client.Ping(ctx).Err()
		if err != nil {
			log.Print("Ping:", err)
			return err
		}
	}
	return err
}

func (r *RedisHandler) PingTopic(ctx context.Context) error {
	err := r.topic.Ping(ctx)

	if err != nil {
		log.Printf("Redis not responding to %s retry in 3 seconds err: %s", r.topic.String(), err)

		time.Sleep(time.Second * 3)

		err := r.topic.Ping(ctx)
		if err != nil {
			log.Print("Ping:", err)
			return err
		}
	}

	return err
}

func (r *RedisHandler) ReadQueue(ctx context.Context, topic string, conn *websocket.Conn) {

	err := r.PingClient(ctx)
	if err != nil {
		log.Println(err)
		conn.WriteMessage(websocket.CloseMessage, []byte("Internal Error"))
		return
	}

	err = r.Subscribe(ctx, topic)
	if err != nil {
		log.Println(err)
		conn.WriteMessage(websocket.CloseMessage, []byte("Internal Error"))
		return
	}

	channel := r.topic.Channel()
	closeChan := make(chan error, 1)

	go listenForConnectionClose(conn, closeChan)
	go readRedisChannel(channel, conn)

	err = <-closeChan
	if err != nil {
		conn.Close()
		log.Print(err)
		log.Print("Client Disconnected")
	}

}

func readRedisChannel(channel <-chan *redis.Message, conn *websocket.Conn) {
	for msg := range channel {
		if err := conn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {
			log.Println(err)
			return
		}
	}
}

func listenForConnectionClose(conn *websocket.Conn, channel chan<- error) {
	for {
		_, _, err := conn.ReadMessage()
		if c, ok := err.(*websocket.CloseError); ok {
			if c.Code == 1005 {
				channel <- fmt.Errorf("connection closed from client")
				return
			}
		}
		if err != nil {
			channel <- err
			return
		}
	}
}

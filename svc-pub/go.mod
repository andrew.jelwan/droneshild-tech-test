module svc-pub

go 1.17

require (
	github.com/alicebob/miniredis/v2 v2.15.1
	github.com/go-redis/redis/v8 v8.11.3
	github.com/stretchr/testify v1.5.1
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

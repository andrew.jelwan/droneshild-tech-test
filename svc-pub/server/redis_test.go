package server

import (
	"context"
	"encoding/json"
	"fmt"
	"svc-pub/drone"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
)

func mockRedis() *miniredis.Miniredis {
	rmock, err := miniredis.Run()
	rmock.RequireAuth("test")

	if err != nil {
		panic(err)
	}
	return rmock
}

func NewMockRedisHandler(ctx context.Context, address string, password string) *RedisHandler {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       0,
	})

	return &RedisHandler{
		ctx:    context.Background(),
		Client: client,
		Topic:  "test",
	}
}

func TestClientFail(t *testing.T) {
	mockRedisServer := mockRedis()
	ctx := context.Background()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	mockRedisServer.SetError("Mock Error")
	err := redisHandler.Ping()
	assert.EqualError(t, err, "Mock Error")
	mockRedisServer.SetError("")
}

func TestClientSuccess(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")
	err := redisHandler.Ping()
	assert.NoError(t, err)
}

func TestSendCoordiantes(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")

	sub := mockRedisServer.NewSubscriber()
	sub.Subscribe(redisHandler.Topic)
	channel := sub.Messages()
	done := make(chan error, 1)

	testCoord := drone.Coordinate{Latitude: 0.0001, Longitude: 0.0001}

	go func(channel <-chan miniredis.PubsubMessage, done chan<- error) {
		for i := 0; i < 3; i++ {
			msg := <-channel
			coord := &drone.Coordinate{}
			_ = json.Unmarshal([]byte(msg.Message), &coord)
			if *coord != testCoord {
				done <- fmt.Errorf("Coordiantes not equal")
			}
		}
		done <- nil
	}(channel, done)

	for i := 0; i < 3; i++ {
		err := redisHandler.SendCoordiantes(testCoord)
		assert.NoError(t, err)
	}

	err := <-done
	assert.NoError(t, err)
}

func TestSendCoordiantesError(t *testing.T) {
	ctx := context.Background()
	mockRedisServer := mockRedis()
	redisHandler := NewMockRedisHandler(ctx, mockRedisServer.Addr(), "test")

	mockRedisServer.SetError("Mock Error")

	testCoord := drone.Coordinate{Latitude: 0.0001, Longitude: 0.0001}

	err := redisHandler.SendCoordiantes(testCoord)
	assert.EqualError(t, err, "Mock Error")

}

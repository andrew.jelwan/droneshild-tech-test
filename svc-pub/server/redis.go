package server

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"svc-pub/drone"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisHandler struct {
	ctx    context.Context
	Client *redis.Client
	Topic  string
}

func NewRedisHandler(ctx context.Context, address string, password string, topic string) *RedisHandler {
	return &RedisHandler{
		ctx: ctx,
		Client: redis.NewClient(&redis.Options{
			Addr:     address,
			Password: password,
			DB:       0,
		}),
		Topic: topic,
	}
}

// Ping Redis to make sure the service is up before sending messages
func (r *RedisHandler) Ping() error {
	err := r.Client.Ping(r.ctx).Err()
	if err != nil {
		fmt.Printf("Redis not responding retry in 3 seconds err: %s", err)
		time.Sleep(time.Second * 3)
		err := r.Client.Ping(context.Background()).Err()
		if err != nil {
			log.Print("Ping:", err)
			return err
		}
	}
	return err
}

// send provided coordinates to redis pubsub
func (r *RedisHandler) SendCoordiantes(coordiante drone.Coordinate) error {
	message, err := json.Marshal(coordiante)
	if err != nil {
		return err
	}
	redisErr := r.Client.Publish(r.ctx, r.Topic, message)
	return redisErr.Err()
}

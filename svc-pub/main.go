package main

import (
	"context"
	"fmt"
	"log"
	"svc-pub/drone"
	"svc-pub/server"
	"time"
)

type Publisher struct {
	ctx         context.Context
	redisHelper *server.RedisHandler
}

func NewPublisher() *Publisher {
	return &Publisher{
		ctx:         context.Background(),
		redisHelper: server.NewRedisHandler(context.Background(), "redis:6379", "password", "detection"),
	}
}

func main() {
	fmt.Println("Starting Redis Publisher....")

	publisher := NewPublisher()
	// using a blocking channel so the output is limited by the sleep time
	movement := make(chan drone.Coordinate, 1)

	// New drone object to use for all the movement data
	drone := drone.NewDrone(drone.Coordinate{
		Latitude:  -33.9427089,
		Longitude: 151.1743063,
	})

	// Selectable drone flight paths
	path := "circle"

	switch path {
	case "zigzag":
		go drone.ZigZagFlightPath(movement)
	case "cross":
		go drone.CrossFlightPath(movement)
	case "circle":
		go drone.CircleFlightPath(movement, 0.0010)
	case "airport":
		go drone.AirportPath(movement)
	}

	// Using an ininate loop for to send all the drone coordinates
	for {
		move := <-movement
		err := publisher.redisHelper.SendCoordiantes(move)
		if err != nil {
			log.Print("SendMessage:", err)
			return
		}
		time.Sleep(time.Second * 1)
	}
}

package drone

import "testing"

func TestNewDrone(t *testing.T) {
	testCoord := Coordinate{Latitude: 0.0001, Longitude: 0.0001}
	drone := NewDrone(testCoord)

	if drone.Coordiantes != testCoord {
		t.Fatal("coordiantes are diffrent")
	}
}

func TestGetCoordiantes(t *testing.T) {
	testCoord := Coordinate{Latitude: 0.0001, Longitude: 0.0001}
	drone := NewDrone(testCoord)

	if drone.GetCoordiantes() != testCoord {
		t.Fatal("coordiantes are diffrent")
	}
}

func TestMoveDrone(t *testing.T) {
	testCoord := Coordinate{Latitude: 0.0001, Longitude: 0.0001}
	drone := NewDrone(testCoord)
	drone.MoveDrone()
}

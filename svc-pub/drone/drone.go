package drone

import "math"

// This package is for all the drone related movements and flight paths

type Drone struct {
	Coordiantes Coordinate
}

type Coordinate struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func NewDrone(coordiantes Coordinate) *Drone {
	return &Drone{Coordiantes: coordiantes}
}

func (d *Drone) GetCoordiantes() Coordinate {
	return d.Coordiantes
}

func (d *Drone) MoveDrone() {
	d.Coordiantes.Latitude += 0.00009173
	d.Coordiantes.Longitude += 0.00009173
}

func (d *Drone) MoveNorth(distance float64) {
	d.Coordiantes.Latitude += distance
}

func (d *Drone) MoveNorthEast(distance float64) {
	d.MoveNorth(distance)
	d.MoveEast(distance)
}

func (d *Drone) MoveEast(distance float64) {
	d.Coordiantes.Longitude += distance
}

func (d *Drone) MoveSouthEast(distance float64) {
	d.MoveSouth(distance)
	d.MoveEast(distance)
}

func (d *Drone) MoveSouth(distance float64) {
	d.Coordiantes.Latitude -= distance
}

func (d *Drone) MoveSouthWest(distance float64) {
	d.MoveSouth(distance)
	d.MoveWest(distance)
}

func (d *Drone) MoveWest(distance float64) {
	d.Coordiantes.Longitude -= distance
}

func (d *Drone) MoveNorthWest(distance float64) {
	d.MoveNorth(distance)
	d.MoveWest(distance)
}

func (d *Drone) ZigZagFlightPath(channel chan<- Coordinate) {
	for {
		d.MoveNorthEast(0.00009173)
		channel <- d.GetCoordiantes()
		d.MoveNorthWest(0.00009173)
		channel <- d.GetCoordiantes()
	}
}

func (d *Drone) CrossFlightPath(channel chan<- Coordinate) {
	for {
		for i := 0; i < 20; i++ {
			d.MoveWest(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 5; i++ {
			d.MoveNorth(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveEast(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveNorth(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 5; i++ {
			d.MoveEast(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveSouth(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveEast(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 5; i++ {
			d.MoveSouth(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveWest(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveSouth(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 5; i++ {
			d.MoveWest(0.0001)
			channel <- d.GetCoordiantes()
		}
		for i := 0; i < 20; i++ {
			d.MoveNorth(0.0001)
			channel <- d.GetCoordiantes()
		}
	}
}

func (d *Drone) CircleFlightPath(channel chan<- Coordinate, radius float64) {
	for i := 0.0; i < 360; i += 18 {
		channel <- Coordinate{
			Latitude:  d.Coordiantes.Latitude + (math.Cos(i) * radius),
			Longitude: d.Coordiantes.Longitude + (math.Sin(i) * radius),
		}
		if i >= 342 {
			i = 0.0
		}
	}
}

func (d *Drone) AirportPath(channel chan<- Coordinate) {

	coords := []Coordinate{
		{-33.941271641452, 151.17401562481},
		{-33.941912493258, 151.17159090786},
		{-33.942419178705, 151.16944749893},
		{-33.942908828049, 151.16734694497},
		{-33.943353855978, 151.16549085633},
		{-33.943692075648, 151.16394590394},
		{-33.942882126299, 151.16353820817},
		{-33.942054367903, 151.16316269891},
		{-33.941778446649, 151.1631305124},
		{-33.94129780749, 151.1650938894},
		{-33.940808264816, 151.16761516587},
		{-33.940176305566, 151.17023300187},
		{-33.939838071928, 151.17190670029},
		{-33.939731261026, 151.17235731141},
		{-33.938502926023, 151.1719388868},
		{-33.93710351314, 151.17157837079},
		{-33.935109623506, 151.17108484433},
		{-33.933801108061, 151.17072006391},
		{-33.932786327138, 151.16975446866},
		{-33.931237427678, 151.16920729802},
		{-33.930035675932, 151.16896053479},
		{-33.929724107896, 151.17019435094},
		{-33.929519362567, 151.17180367635},
		{-33.929670696119, 151.17332717107},
		{-33.929991166283, 151.17414256261},
		{-33.931531186637, 151.17440005467},
		{-33.93352516006, 151.17496868298},
		{-33.935039471981, 151.17517316949},
		{-33.937344904551, 151.17575252663},
		{-33.938751276777, 151.17613876473},
		{-33.938259241957, 151.1780999527},
		{-33.937894297388, 151.17996677017},
		{-33.937422537457, 151.18189796066},
		{-33.936879565244, 151.1838720665},
		{-33.936559121002, 151.18556722259},
		{-33.936247576833, 151.18697270012},
		{-33.936131860137, 151.18797048187},
		{-33.936843960388, 151.18822797394},
		{-33.937796385165, 151.18847473717},
		{-33.938143527995, 151.18724092102},
		{-33.9386063829, 151.1853311882},
		{-33.939015829372, 151.18368967628},
		{-33.939336264368, 151.18212326622},
		{-33.939781310973, 151.18054612732},
		{-33.940181850929, 151.17862566566},
		{-33.940537884863, 151.17734893417},
		{-33.940813810138, 151.17660864448},
		{-33.941921699228, 151.17700561142},
		{-33.943292393692, 151.17737039184},
		{-33.944710105919, 151.17796171545},
		{-33.946071855234, 151.17857325911},
		{-33.947396382024, 151.17901039545},
		{-33.948633488688, 151.17948246423},
		{-33.949754879191, 151.17978287164},
		{-33.951036450244, 151.18023348276},
		{-33.951368921293, 151.17906284939},
		{-33.951564714407, 151.17773247372},
		{-33.951288823981, 151.17737842213},
		{-33.949304166496, 151.17665959012},
		{-33.947527190758, 151.17609055775},
		{-33.945827254884, 151.17572577732},
		{-33.944031098973, 151.17521949108},
		{-33.941627939634, 151.17453284557},
		{-33.941271641452, 151.17401562481},
	}

	for _, c := range coords {
		channel <- c
	}
}

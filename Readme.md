# Droneshield Tech test

This project can be brought up via Docker-Compose which will bring all the services up as well as Redis and has the networking set up.

Run the following in the project root:
```
docker-compose up --build
```

## svc-pub
This micro-service is used to simulate drone movements and send them to the Redis PubSub Queue. 

The messages have a one second wait between them as to not send an unreasonable amount of messages.

There are multiple flight simulations available they can be selected in the switch statement:
```go
	switch path {
	case "zigzag":
		go drone.ZigZagFlightPath(movement)
	case "cross":
		go drone.CrossFlightPath(movement)
	case "circle":
		go drone.CircleFlightPath(movement, 0.0010)
	case "airport":
		go drone.AirportPath(movement)
	}
```

## svc-sub
This micro-service is used to pipe all the messages from the Redis Queue to a Web-socket

The Web-socket is available on:
```
ws://localhost:8080
```
There is also an echo endpoint on
```
ws://localhost:8080/echo
```

## Diagram

![image](./diagram.svg)

# Thanks
This tech test was a bunch of fun i learnt some new things while completing it 